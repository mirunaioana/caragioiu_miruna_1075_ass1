const FIRST_NAME = "Miruna";
const LAST_NAME = "Caragioiu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function dynamicPropertyChecker(input, property) {

}

function numberParser(nr) {
    if (nr < Number.MIN_VALUE || nr >= Number.MAX_VALUE) {
        return NaN;
    }
    return parseInt(nr);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}